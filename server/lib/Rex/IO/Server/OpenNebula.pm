#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Server::OpenNebula;
   
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON;

use Data::Dumper;

use Net::OpenNebula;

sub list_vms_on_host {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my ($host) = grep { $_->name eq $hw->name } $self->_one->get_hosts;

   if($host) {
      my @vms = map { $_->get_data } $host->vms;
      my @rest = map { $_->get_data } grep { $_->state eq "stopped" || $_->state eq "pending" } $self->_one->get_vms;
      push @vms, @rest;

      return $self->render(json => {ok => Mojo::JSON->true, data => \@vms});
   }


   return $self->render(json => {ok => Mojo::JSON->false}, status => 404);
}

sub has_opennebula {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $one = $hw->opennebula;

   if($one) {
      return $self->render(json => { ok => Mojo::JSON->true });
   }

   return $self->render(json => { ok => Mojo::JSON->false }, status => 404);
}

sub list_templates {
   my ($self) = @_;


   my @templates = map { $_->get_data } $self->_one->get_templates;

   return $self->render(json => { ok => Mojo::JSON->true, data => \@templates });
}

sub new_vm_on_host {
   my ($self) = @_;

   my $ref = $self->req->json;

   my $count = $ref->{count};
   if($count > 1) {
      for(my $i=1; $i <= $count; $i++) {
         $self->_one->create_vm(
            template => $ref->{template},
            name     => $ref->{name} . " ($i)",
         );
      }
   }
   else {
      $self->_one->create_vm(
         template => $ref->{template},
         name     => $ref->{name},
      );
   }

   $self->render(json => {ok => Mojo::JSON->true});
}

sub vm_action {
   my ($self) = @_;

   my $action = $self->param("vm_action");
   
   my $vm = $self->_one->get_vm($self->param("vm_id"));
   $vm->$action();

   return $self->render(json => { ok => Mojo::JSON->true });
}

sub __register__ {
   my ($self, $app) = @_;
   my $r = $app->routes;

   $r->route("/1.0/opennebula/server/:id/vm")->via("LIST")->to("open_nebula#list_vms_on_host");
   $r->route("/1.0/opennebula/server/:id")->via("INFO")->to("open_nebula#has_opennebula");
   $r->post("/1.0/opennebula/server/:id/vm")->to("open_nebula#new_vm_on_host");

   $r->route("/1.0/opennebula/server/:id/template")->via("LIST")->to("open_nebula#list_templates");

   $r->post("/1.0/opennebula/server/:id/vm/:vm_id/:vm_action")->to("open_nebula#vm_action");
}

sub _one {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $one = $hw->opennebula;

   my $rpc = $one->rpc;
   if($rpc =~ m/^auto:/) {
      my $pip = $hw->primary_ip;
      $rpc =~ s/^auto:/$pip:/;
   }

   my $o = Net::OpenNebula->new(url => "http://$rpc/RPC2",
      user     => $one->username,
      password => $one->password);

   return $o;
}

sub __inventor__ {
   my ($class, $hw, $db, $ref) = @_;

   if($ref->{opennebula}) {
      my $rpc = $ref->{opennebula}->{ONE_HOST} . ":" . $ref->{opennebula}->{ONE_PORT};
      my $user = $ref->{opennebula}->{ONE_USER};
      my $pass = $ref->{opennebula}->{ONE_PASS};

      my $one = $db->resultset("OpenNebula")->create({
         rpc => $rpc,
         username => $user,
         password => $pass,
         hardware_id => $hw->id,
      });
   }
}



1;
