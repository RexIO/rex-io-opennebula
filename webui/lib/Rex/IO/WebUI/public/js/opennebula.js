(function() {

   $(document).ready(function() {


   });

})();

var one_table_init_done = 0;

var one_oTable;

function prepare_opennebula_vm_table() {

   if(! $("#table_opennebula_vms_wrapper")[0]) {

      one_oTable = $("#table_opennebula_vms").dataTable({
         "bJQueryUI": true,
         "bPaginate": false,
         "sScrollY": 300,
         "sPaginationType": "full_numbers"
      });

      prepare_data_tables();

      $("#table_opennebula_vms tbody tr").click( function( e ) {
         if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
         }
         else {
            one_oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
         }
      });

      return one_oTable;

   }


}

function list_opennebula_vms(srv_id) {

   $("#add_one_vm").dialog({
      autoOpen: false,
      height: 300,
      width: 400,
      modal: true,
      buttons: {
         "Add": function() {
            $.log("adding");
            
            add_one_vm(srv_id, {
               name: $("#name").val(),
               template: $("#template").val(),
               count: $("#count").val(),
               callback: function() {
                  // reload server view
               }
            });

            $(this).dialog("close");
         },
         Cancel: function() {
            $(this).dialog("close");
         }
      },
      close: function() {
         $("INPUT").val("").removeClass("ui-state-error");
      }
   });

   $("#lnk_one_new_vm").click(function() {
      $("#add_one_vm").dialog("open");
   });

   $(".lnk_one_vm_action").click(function() {
      var selected_row = fnGetSelected(one_oTable);
      var vm_id = $(selected_row).attr("vm_id");
      var srv_id = $(selected_row).attr("srv_id");
      var vm_action = $(this).attr("vm_action");

      $.log(vm_action + ": " + srv_id + "/" + vm_id);

      one_vm_action(srv_id, vm_id, vm_action);
   });

   $("#lnk_one_reload").click(function() {
      one_view_reload(srv_id);
   });


}

function add_one_vm(srv_id, ref) {

   $.ajax({
      "url": "/opennebula/server/" + srv_id + "/vm",
      "type": "POST",
      "data": JSON.stringify({
         "template": ref.template,
         "name": ref.name,
         "count": ref.count
      }),
      "contentType": "application/json",
      "dataType": "json"
   }).done(function(data) {
      try {
         if(data.ok != true) {
            throw "Error adding new dns record";
         }
         else {
            if(typeof ref.callback != "undefined") {
               ref.callback();
            }
         }
      } catch(err) {
         $.log(err);
      }
   });
}

function one_vm_action(srv_id, vm_id, action) {
   $.log(action + ": vm: " + vm_id + " on " + srv_id);

   $.ajax({
      "url": "/opennebula/server/" + srv_id + "/vm/" + vm_id + "/" + action,
      "type": "POST",
      "dataType": "json"
   }).done(function(data) {
      try {
         if(data.ok != true) {
            $.log("Error " + action + " VM");
         }
         else {
            one_view_reload(srv_id);
         }
      } catch(err) {
         $.log(err);
      }
   });
}

function one_view_reload(srv_id) {
   load_server(srv_id, function() {
      activate_tab($("#li-tab-opennebula"));
   });
}

