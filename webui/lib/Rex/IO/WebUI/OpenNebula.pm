#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::WebUI::OpenNebula;

use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use File::Basename 'dirname';

sub new_vm {
   my ($self) = @_;

   my $ref = $self->req->json;

   my $ret = $self->rexio->call(POST => "1.0", "opennebula", server => $self->param("id"), vm => undef, ref => $ref);

   return $self->render(json => $ret);
}

sub vm_action {
   my ($self) = @_;

   my $ret = $self->rexio->call(POST => "1.0", "opennebula", server => $self->param("id"), vm => $self->param("vm_id"), $self->param("vm_action") => undef);

   return $self->render(json => $ret);
}

sub rexio_routes {
   my ($self, $routes) = @_;
   my $r      = $routes->{route};
   my $r_auth = $routes->{route_auth};

   $r_auth->post("/opennebula/server/:id/vm")->to("open_nebula#new_vm");
   $r_auth->post("/opennebula/server/:id/vm/:vm_id/:vm_action")->to("open_nebula#vm_action");
}

sub __init__ {
   my ($class, $app) = @_;

   # add plugin template path
   push(@{ $app->renderer->paths }, dirname(__FILE__) . "/templates");
   push(@{ $app->static->paths }, dirname(__FILE__) . "/public");
}

1;
