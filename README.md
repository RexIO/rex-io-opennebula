## Rex.IO - OpenNebula Plugin

Access your OpenNebula Cloud from within Rex.IO

## Requirements

This Plugin requires Net::OpenNebula Perl module.

## Screenshot

![screenshot](https://raw.github.com/RexIO/rex-io-opennebula/master/doc/list_vms.png)
